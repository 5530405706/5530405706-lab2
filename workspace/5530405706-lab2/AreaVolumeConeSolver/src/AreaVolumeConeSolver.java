
public class AreaVolumeConeSolver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        if(args.length==3) {
                double r  = Double.parseDouble(args[0]);
                double s  = Double.parseDouble(args[1]);
                double h  = Double.parseDouble(args[2]);
                double PI = java.lang.Math.PI;
                double SurfaceArea = (PI*r*s) + (PI*r*r) ; 
                double Volume = (PI*r*r*h)/3 ;
        System.out.println("For cone with r " + r + " s " + s + " h " + h);
        System.out.println("Surface area is " + SurfaceArea + " Volume is " + Volume );
        }
        else System.err.println("AreaVolumeSolver <r> <s> <h>");
        }
}

public class UsingStringAPI {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        String s1 = args[0] ;
        String s2 = s1.toUpperCase() ;

        int schoolName = s2.indexOf("SCHOOL");
        int collegeName = s2.indexOf("COLLEGE");

        if (schoolName == -1 && collegeName == -1) {
        System.out.println(s1 + " does not contain school or college ");
                
        }

        else if (collegeName != -1) {
        System.out.println(s1 + " has college at " + collegeName);
                
        }
        
        else if (schoolName != -1) {
        System.out.println(s1 + " has school at " + schoolName);
        }
        
        
	}
}